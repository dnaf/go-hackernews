package hackernews

import (
	"testing"
)

func testReadFeed(c *Client, feed StoryFeedType) func(t *testing.T) {
	return func(t *testing.T) {
		t.Parallel()
		stories, err2 := c.GetStories(feed)
		if err2 != nil {
			panic(err2)
		}

		for _, story := range stories[:10] {
			details, err3 := story.Details()
			if err3 != nil {
				panic(err3)
			}
			if details.Score < 1 {
				t.Fatal("Post scores shouldn't be able to be less than 1")
			}
		}

	}
}

func TestReadingPosts(t *testing.T) {
	c, err := NewClient()
	if err != nil {
		panic(err)
	}

	t.Run("Top stories", testReadFeed(c, TopStories))
	t.Run("Best stories", testReadFeed(c, BestStories))
	t.Run("New stories", testReadFeed(c, NewStories))
}
