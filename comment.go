package hackernews

import (
	"encoding/json"
	"time"
)

// Comment represents a text reply to a story or another comment.
type Comment struct {
	id     uint
	loaded bool
	client *Client

	details CommentDetails
}

// CommentDetails holds information about a comment.
type CommentDetails struct {
	Parent  Item       // pointer to a comment or story that this comment is a reply to
	Author  string     // the author of the comment
	Replies []*Comment // direct replies to this comment
	Text    string     // the comment's text
	Time    time.Time  // the time at which the comment was posted
}

// GetComment retrieves a comment with the specified ID over the API.
// This function won't fail if the given ID isn't a comment. For safer
// usage, use client.GetItem()
func (client *Client) GetComment(id uint) *Comment {
	return &Comment{
		id:     id,
		loaded: false,
		client: client,
	}
}

func (c *Comment) load() error {
	body, err := c.client.getItem(c.id)
	if err != nil {
		return err
	}

	return c.loadData(body)
}

func (c *Comment) loadData(body []byte) error {
	v := struct {
		By, Text string
		Parent   uint
		Time     int64
		Kids     []uint
	}{}

	err2 := json.Unmarshal(body, &v)
	if err2 != nil {
		return err2
	}

	c.details.Author = v.By
	c.details.Text = v.Text
	c.details.Parent, err2 = c.client.GetItem(v.Parent)
	if err2 != nil {
		return err2
	}
	c.details.Time = time.Unix(v.Time, 0)

	c.details.Replies = make([]*Comment, 0, len(v.Kids))
	for _, id := range v.Kids {
		c.details.Replies = append(c.details.Replies, &Comment{
			id:     id,
			loaded: false,
			client: c.client,
		})
	}

	c.loaded = true

	return nil
}

// Details returns information about a Comment. This function is blocking. If the comment's details haven't already been requested, it may take time for this function to retrieve them.
func (c *Comment) Details() (*CommentDetails, error) {
	if !c.loaded {
		err := c.load()
		if err != nil {
			return nil, err
		}
	}

	return &c.details, nil
}

// ID returns the ID of the comment.
func (c *Comment) ID() uint {
	return c.id
}

// Client returns the client this comment is attached to.
func (c *Comment) Client() *Client {
	return c.client
}
