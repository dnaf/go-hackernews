package hackernews

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

// Client represents a client of the Hacker News API. All client methods are safe for concurrent use by goroutines.
type Client struct {
	http *http.Client
	root *url.URL
}

// NewClient creates a new Hacker News client.
func NewClient() (*Client, error) {
	c := &Client{}
	var err error
	c.root, err = url.Parse("https://hacker-news.firebaseio.com/v0/")
	if err != nil {
		return nil, err
	}

	c.http = &http.Client{}

	return c, nil
}

func (c *Client) getActionURL(action string) (*url.URL, error) {
	u, err := url.Parse(action + ".json")
	if err != nil {
		return nil, err
	}

	return c.root.ResolveReference(u), nil
}
func (c *Client) getItemURL(id uint) (*url.URL, error) {
	return c.getActionURL("item/" + strconv.FormatUint(uint64(id), 10))
}

func (c *Client) get(u *url.URL) ([]byte, error) {
	resp, err := c.http.Get(u.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err2 := ioutil.ReadAll(resp.Body)
	return body, err2
}

func (c *Client) getAction(action string) ([]byte, error) {
	u, err := c.getActionURL(action)
	if err != nil {
		return nil, err
	}
	return c.get(u)
}

func (c *Client) getItem(id uint) ([]byte, error) {
	u, err := c.getItemURL(id)
	if err != nil {
		return nil, err
	}
	return c.get(u)
}
