package hackernews

import (
	"fmt"
)

func ExampleClient_GetStories() {
	// Create new client
	// It's extremely unlikely for an error to happen
	// when creating a client, so I'm just going to ignore
	// it for this example
	c, _ := NewClient()

	// Get the top stories at the moment
	stories, err := c.GetStories(TopStories)
	if err != nil {
		panic(err)
	}

	// Print out the top 5 stories
	for _, story := range stories[:5] {
		// Get story details
		details, err2 := story.Details()
		if err2 != nil {
			panic(err2)
		}

		// Print name, score, and poster
		fmt.Printf("%v\n%v points\tby %v\n\n", details.Title, details.Score, details.Author)
	}
}

func ExampleClient_GetStory() {
	// Create new client
	// It's extremely unlikely for an error to happen
	// when creating a client, so I'm just going to ignore
	// it for this example
	c, _ := NewClient()

	// Get a story with a specific ID
	//
	// Be careful what IDs you pass to this function;
	// it doesn't return any errors of the ID points to
	// a different type of item or if the ID doesn't exist.
	//
	// For safety, use Client.GetItem() if you aren't 100% sure
	// the ID points to a story.
	story := c.GetStory(17221527)

	// Get story details
	details, err := story.Details()
	if err != nil {
		panic(err)
	}

	// Print the title and URL of the story
	fmt.Printf("title: %s\nlink: %s", details.Title, details.URL.String())

	// Output:
	// title: Microsoft Is Said to Have Agreed to Acquire GitHub
	// link: http://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github?
}
