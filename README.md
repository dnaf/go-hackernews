# go-hackernews

**Read-only access to the HN API in Go**

[![GoDoc](https://godoc.org/gitlab.com/dnaf/go-hackernews?status.svg)](https://godoc.org/gitlab.com/dnaf/go-hackernews)
[![CI status](https://gitlab.com/dnaf/go-hackernews/badges/master/build.svg)](https://gitlab.com/dnaf/go-hackernews/pipelines)

See the [docs](https://godoc.org/gitlab.com/dnaf/go-hackernews) for usage information and examples.