package hackernews

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
)

// Item represents a story or comment. It must be cast to a story or comment to be useful.
type Item interface {
	ID() uint        // returns the ID of the item
	Client() *Client // returns the client this item is attached to
}

// YCombURL returns a link to this item on news.ycombinator.com
func YCombURL(item Item) *url.URL {
	u, err := url.Parse(fmt.Sprintf("https://news.ycombinator.com/item?id=%d", item.ID()))
	if err != nil {
		panic(err)
	}
	return u
}

// GetItem returns an item of arbitrary type from the API.
func (c *Client) GetItem(id uint) (Item, error) {
	body, err := c.getItem(id)
	if err != nil {
		return nil, err
	}

	t := struct {
		Type string
	}{}

	json.Unmarshal(body, &t)

	switch t.Type {
	case "story":
		item := c.GetStory(id)
		return item, item.loadData(body)
	case "comment":
		item := c.GetComment(id)
		return item, item.loadData(body)
	}
	return nil, errors.New("Unknown item type \"" + t.Type + "\"")
}
