package hackernews

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"
)

// StoryFeedType represents a story feed sort order. It can be one of `TopStories`, `NewStories`, or `BestStories`.
type StoryFeedType byte

// Possible StoryFeedType values
const (
	TopStories StoryFeedType = iota
	NewStories
	BestStories
)

func (s StoryFeedType) endpoint() string {
	switch s {
	case TopStories:
		return "topstories"
	case NewStories:
		return "newstories"
	case BestStories:
		return "beststories"
	}
	panic(fmt.Sprintf("Invalid StoryFeedType `%v`", s))
}

// Story represents a story grabbed from the Hacker News API. Its information can be accessed by the `Details()` method.
type Story struct {
	id     uint
	loaded bool
	client *Client

	details StoryDetails
}

// StoryDetails represents a Story's information.
type StoryDetails struct {
	Title         string     // the post's title
	Author        string     // the author of the post
	URL           *url.URL   // the url the post links to
	Score         int        // the post's score
	Comments      []*Comment // the post's top-level comments
	TotalComments int        // the total number of comments the post has
	Time          time.Time  // the time the post was made
}

func (c *Client) getStoryIDs(feed StoryFeedType) ([]uint, error) {
	body, err := c.getAction(feed.endpoint())
	if err != nil {
		return nil, err
	}

	// Parse JSON
	var ids []uint
	if err2 := json.Unmarshal(body, &ids); err2 != nil {
		return nil, err2
	}

	return ids, nil
}

// GetStory retrieves a comment with the specified ID over the API.
// This function won't fail if the given ID isn't a story. For safer
// usage, use client.GetItem()
func (c *Client) GetStory(id uint) *Story {
	return &Story{
		id:     id,
		loaded: false,
		client: c,
	}
}

// GetStories returns stories sorted by the given StoryFeedType. This function is blocking.
func (c *Client) GetStories(feed StoryFeedType) ([]*Story, error) {
	ids, err := c.getStoryIDs(feed)
	if err != nil {
		return nil, err
	}

	stories := make([]*Story, 0, len(ids))
	for _, id := range ids {
		stories = append(stories, &Story{
			id:     id,
			loaded: false,
			client: c,
		})
	}

	return stories, nil
}

func (s *Story) load() error {
	body, err := s.client.getItem(s.id)
	if err != nil {
		return err
	}

	return s.loadData(body)
}

func (s *Story) loadData(body []byte) error {
	v := struct {
		By, Title, URL string
		Descendants    int
		Score          int
		Time           int64
		Kids           []uint
	}{}

	err2 := json.Unmarshal(body, &v)
	if err2 != nil {
		return err2
	}

	s.details.Author = v.By
	s.details.Title = v.Title
	s.details.Score = v.Score
	s.details.TotalComments = v.Descendants
	s.details.Time = time.Unix(v.Time, 0)

	var err3 error
	s.details.URL, err3 = url.Parse(v.URL)
	if err3 != nil {
		return err3
	}

	s.details.Comments = make([]*Comment, 0, len(v.Kids))
	for _, id := range v.Kids {
		s.details.Comments = append(s.details.Comments, &Comment{
			id:     id,
			loaded: false,
			client: s.client,
		})
	}

	s.loaded = true

	return nil
}

// Details returns information about a Story. This function is blocking. If the story's details haven't already been requested, it may take time for this function to retrieve them.
func (s *Story) Details() (*StoryDetails, error) {
	if !s.loaded {
		err := s.load()
		if err != nil {
			return nil, err
		}
	}

	return &s.details, nil
}

// ID returns the ID of the story.
func (s *Story) ID() uint {
	return s.id
}

// Client returns the client this story is attached to.
func (s *Story) Client() *Client {
	return s.client
}
