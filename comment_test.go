package hackernews

import (
	"fmt"
	"testing"
)

type ctc struct { // comment test case
	id     uint
	author string
	text   string
}

func testSpecificComment(client *Client, tc ctc) func(t *testing.T) {
	return func(t *testing.T) {
		c := client.GetComment(tc.id)

		details, err := c.Details()
		if err != nil {
			panic(err)
		}

		if details.Text != tc.text {
			panic(fmt.Sprintf("Text does not match! Expected \"%q\", got \"%q\"", tc.text, details.Text))
		}
	}
}

func TestSpecificComments(t *testing.T) {
	c, err := NewClient()
	if err != nil {
		panic(err)
	}

	cases := [...]ctc{
		{2921983, "norvig", "Aw shucks, guys ... you make me blush with your compliments.<p>Tell you what, Ill make a deal: I'll keep writing if you keep reading. K?"},
		{29259, "aston", "Awesome."},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("Comment %d", tc.id), testSpecificComment(c, tc))
	}
}
