package hackernews

import (
	"fmt"
	"testing"
)

func getItemChk(c *Client, id uint) Item {
	item, err := c.GetItem(id)
	if err != nil {
		panic(err)
	}
	return item
}

func testGetItemComment(c *Client, id uint) func(t *testing.T) {
	return func(t *testing.T) {
		_, ok := getItemChk(c, id).(*Comment)
		if !ok {
			panic("Item is invalid type")
		}
	}
}
func testGetItemStory(c *Client, id uint) func(t *testing.T) {
	return func(t *testing.T) {
		_, ok := getItemChk(c, id).(*Story)
		if !ok {
			panic("Item is invalid type")
		}
	}
}

func TestGetItem(t *testing.T) {
	c, err := NewClient()
	if err != nil {
		panic(err)
	}

	comments := []uint{17221777, 2921983, 29259}
	for _, id := range comments {
		t.Run(fmt.Sprintf("GetItem.(Comment) %d", id), testGetItemComment(c, id))
	}
}

func ExampleYCombURL() {
	// Create a new client
	client, err := NewClient()
	if err != nil {
		panic(err)
	}

	// Get a specific item by ID
	item, err2 := client.GetItem(29259)
	if err2 != nil {
		panic(err)
	}

	// Print that item's news.ycombinator.com URL
	url := YCombURL(item)
	fmt.Println(url.String())

	// Output: https://news.ycombinator.com/item?id=29259
}
