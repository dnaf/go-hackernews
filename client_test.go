package hackernews

import (
	"fmt"
)

func ExampleClient_GetItem() {
	// Create client
	client, err := NewClient()
	if err != nil {
		panic(err)
	}

	// Get a specific item
	// Let's write a function that can print some
	// information about any item, given an ID.
	printItem := func(id uint) {
		// First, we need to actually retrieve the item
		item, err := client.GetItem(id)
		if err != nil {
			panic(err)
		}

		// Now let's figure out what type it is
		switch v := item.(type) {
		case *Story: // Awesome, it's a story!
			fmt.Println("Story:") // Let's make this clear
			// Let's get its details
			details, err3 := v.Details()
			if err3 != nil {
				panic(err3)
			}

			// And let's print its title!
			fmt.Println(details.Title)

		case *Comment: // Neato, it's a comment!
			fmt.Println("Comment:") // Let's make this clear
			// Let's get its details
			details, err3 := v.Details()
			if err3 != nil {
				panic(err3)
			}

			// And let's print its title!
			fmt.Println(details.Text)
		}

		fmt.Println() // An extra newline to make it look nice
	}

	// Now let's pick some random IDs to test this with
	items := [...]uint{8863, 29259, 2921983, 121003}

	// Finally, lets run printItem on all of them
	for _, item := range items {
		printItem(item)
	}

	// Output:
	// Story:
	// My YC app: Dropbox - Throw away your USB drive
	//
	// Comment:
	// Awesome.
	//
	// Comment:
	// Aw shucks, guys ... you make me blush with your compliments.<p>Tell you what, Ill make a deal: I'll keep writing if you keep reading. K?
	//
	// Story:
	// Ask HN: The Arc Effect

}
